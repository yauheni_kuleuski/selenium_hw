
from selenium import webdriver
from locators.pastebin_locators import LoginPageLocators, CreatePageLocators, PostPageLocators
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os


class BasePastebinPage:

    def __init__(self, driver: webdriver):
        self.name = os.environ.get("username")
        self.passwd = os.environ.get("password")
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)

    def login(self):
        self.driver.find_element_by_xpath(LoginPageLocators.xpath_sign_in_button).click()
        self.wait.until(EC.visibility_of_element_located((By.XPATH,
                                                          LoginPageLocators.xpath_login_block_title)))
        self.driver.find_element_by_id(LoginPageLocators.id_login_username_field).clear()
        self.driver.find_element_by_id(LoginPageLocators.id_login_username_field).send_keys(self.name)
        self.driver.find_element_by_id(LoginPageLocators.id_login_passwd_field).clear()
        self.driver.find_element_by_id(LoginPageLocators.id_login_passwd_field).send_keys(self.passwd)
        self.driver.find_element_by_xpath(LoginPageLocators.xpath_login_button).click()
        self.wait.until(EC.visibility_of_element_located((By.XPATH,
                                                          LoginPageLocators.xpath_logged_user_name)))

class PastebinCreatePage(BasePastebinPage):

    def open_create_post_page(self):
        self.driver.find_element_by_xpath(CreatePageLocators.xpath_open_create_post_page).click()

    def add_post_text(self, text_to_type: str):
        self.driver.find_element_by_xpath(CreatePageLocators.xpath_textarea)\
            .send_keys(text_to_type)

    def add_post_text_highlighting(self, highlighting_type: str):
        self.driver.find_element_by_xpath(CreatePageLocators.xpath_highlighting_list).click()
        full_xpath = CreatePageLocators.get_highlighting_choice_xpath(highlighting_type)
        self.wait.until(EC.visibility_of_element_located((By.XPATH, full_xpath)))
        self.driver.find_element_by_xpath(full_xpath).click()

    def add_post_expiration(self, expiration_value: str):
        self.driver.find_element_by_xpath(CreatePageLocators.xpath_expiration_list).click()
        full_xpath = CreatePageLocators.get_expiration_choice_xpath(expiration_value)
        self.wait.until(EC.visibility_of_element_located((By.XPATH, full_xpath)))
        self.driver.find_element_by_xpath(full_xpath).click()

    def add_post_title_name(self, title_name: str):
        self.driver.find_element_by_xpath(CreatePageLocators.xpath_title_name_field)\
            .send_keys(title_name)

    def create_new_post(self):
        self.driver.find_element_by_xpath(CreatePageLocators.xpath_create_new_post).click()
        self.wait.until(EC.visibility_of_element_located(
            (By.XPATH, PostPageLocators.xpath_post_view_block)))


class PastebinPostPage(BasePastebinPage):

    def check_is_post_create(self) -> bool:
        post_info = self.driver.find_elements_by_xpath(PostPageLocators.xpath_post_view_block)
        return bool(post_info)

    def get_post_view_title_name(self) -> str:
        return self.driver.find_element_by_xpath(PostPageLocators.xpath_post_view_title_name).text

    def get_post_view_expiration(self) -> str:
        return self.driver.find_element_by_xpath(PostPageLocators.xpath_post_view_expiration).text

    def get_post_view_highlighting(self, highlighting_type: str) -> bool:
        results = self.driver.find_elements_by_xpath(
            PostPageLocators.get_highlighting_test_box_xpath(highlighting_type))
        return bool(results)

    def get_post_view_text(self):
        return self.driver.find_element_by_xpath(PostPageLocators.xpath_post_view_text).text
