

class LoginPageLocators:

    xpath_sign_in_button = "//a[@class='btn-sign sign-in' and text()='Login']"
    xpath_login_block_title = "//div[@class='content__title' and text()='Login Page']"
    id_login_username_field = "loginform-username"
    id_login_passwd_field = "loginform-password"
    xpath_login_button = "//div[@class='form_frame_left']/div[contains(@class, 'form-btn-container')]/button"
    xpath_logged_user_name = "//div[@class='header__user-name']"

class CreatePageLocators:

    xpath_open_create_post_page = "//div[@class='header__left']/a[@class='header__btn']"
    xpath_textarea = "//textarea[@id='postform-text' and @name='PostForm[text]']"
    xpath_expiration_list = "//span[@id='select2-postform-expiration-container' " \
                            "and @class='select2-selection__rendered']"
    xpath_expiration_choice = "//ul[@id='select2-postform-expiration-results' " \
                              "and @class='select2-results__options']/li[text()='{expiration_value}']"
    xpath_highlighting_list = "//span[@id='select2-postform-format-container' "\
                              "and @class='select2-selection__rendered']"
    xpath_highlighting_choice = "//ul[@id='select2-postform-format-results' "\
                                "and @class='select2-results__options']/li[@class='select2-results__option']" \
                                "/ul/li[text()='{highlighting_type}']"

    xpath_title_name_field = "//input[@id='postform-name' and @class='form-control']"
    xpath_create_new_post = "//div[contains(@class, 'form-btn-container')]/button[text()='Create New Paste']"

    @classmethod
    def get_expiration_choice_xpath(cls, expiration_value: str) -> str:
        return cls.xpath_expiration_choice.format(expiration_value=expiration_value)

    @classmethod
    def get_highlighting_choice_xpath(cls, highlighting_type: str) -> str:
        return cls.xpath_highlighting_choice.format(highlighting_type=highlighting_type)


class PostPageLocators:
    xpath_post_view_block = "//div[@class='content']/div[@class='post-view']"

    xpath_post_view_title_name = "//div[@class='post-view']/div[@class='details']" \
                                 "/div[@class='info-bar']/div[@class='info-top']/h1"
    xpath_post_view_expiration = "//div[@class='post-view']/div[@class='details']" \
                                 "/div[@class='info-bar']/div[@class='info-bottom']" \
                                 "/div[@class='expire']"
    xpath_post_view_text = "//div[@class='post-view']/textarea[@class='textarea']"
    xpath_highlighted_text_box = "//div[@class='highlighted-code']/div[@class='source']/ol[@class='{highlighting_type}']"

    @classmethod
    def get_highlighting_test_box_xpath(cls, highlighting_type: str) -> str:
        return cls.xpath_highlighted_text_box.format(highlighting_type=highlighting_type.lower())