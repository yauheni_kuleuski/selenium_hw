import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from pages.pastebin import PastebinCreatePage, PastebinPostPage


class GitHistoryPasteTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36")
        chrome_options.add_experimental_option('prefs', {'intl.accept_languages': 'en,en_US'})

        cls.driver = webdriver.Chrome(options=chrome_options,
                                       executable_path=ChromeDriverManager().install())
        cls.driver.get("https://pastebin.com/")
        cls.create_page = PastebinCreatePage(cls.driver)
        cls.post_page = PastebinPostPage(cls.driver)
        cls.expected_data = {
            "post_title_name": "how to gain dominance among developers",
            "post_expiration": "10 Minutes",
            "post_highlighting": "Bash",
            "post_text": """
git config --global user.name  "New Sheriff in Town"
git reset $(git commit-tree HEAD^{tree} -m "Legacy code")
git push origin master --force
            """,
        }

    def test_is_git_post_created(self):
        self.create_page.open_create_post_page()
        self.create_page.add_post_text(self.expected_data['post_text'])
        self.create_page.add_post_expiration(self.expected_data['post_expiration'])
        self.create_page.add_post_text_highlighting(self.expected_data['post_highlighting'])
        self.create_page.add_post_title_name(self.expected_data['post_title_name'])
        self.create_page.create_new_post()
        page_is_created = self.post_page.check_is_post_create()
        self.assertEqual(page_is_created, True)

    def test_git_post_title(self):
        title_name = self.post_page.get_post_view_title_name()
        self.assertEqual(title_name, self.expected_data['post_title_name'])

    def test_git_post_expiration(self):
        expiration = self.post_page.get_post_view_expiration()
        self.assertIn(expiration.lower(), self.expected_data['post_expiration'].lower())

    def test_git_post_text(self):
        post_text = self.post_page.get_post_view_text()
        self.assertEqual(post_text, self.expected_data['post_text'])

    def test_git_post_text_is_highlighted(self):
        highlighted_box_find_results = self.post_page.get_post_view_highlighting(
            self.expected_data['post_highlighting'])
        self.assertEqual(highlighted_box_find_results, True)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.close()


def suite():
    suite = unittest.TestSuite()
    suite.addTest(GitHistoryPasteTestCase('test_is_git_post_created'))
    suite.addTest(GitHistoryPasteTestCase('test_git_post_title'))
    suite.addTest(GitHistoryPasteTestCase('test_git_post_expiration'))
    suite.addTest(GitHistoryPasteTestCase('test_git_post_text'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
