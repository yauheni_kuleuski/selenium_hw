import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from pages.pastebin import PastebinCreatePage, PastebinPostPage


class SimplePasteTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_experimental_option('prefs', {'intl.accept_languages': 'en,en_US'})

        cls.driver = webdriver.Chrome(options=chrome_options,
                                       executable_path=ChromeDriverManager().install())
        cls.driver.get("https://pastebin.com/")
        cls.create_page = PastebinCreatePage(cls.driver)
        cls.post_page = PastebinPostPage(cls.driver)
        cls.expected_data = {
            "post_title_name": "helloweb",
            "post_expiration": "10 Minutes",
            "post_text": "Hello from WebDriver",
        }

    def test_is_simple_post_created(self):
        self.create_page.open_create_post_page()
        self.create_page.add_post_text(self.expected_data['post_text'])
        self.create_page.add_post_expiration(self.expected_data['post_expiration'])
        self.create_page.add_post_title_name(self.expected_data['post_title_name'])
        self.create_page.create_new_post()
        page_is_created = self.post_page.check_is_post_create()
        self.assertEqual(page_is_created, True)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.close()